# dividiPDF #

Poche righe di python per suddividere in molti file un singolo PDF
nel quale sono stati digitalizzati diversi documenti in un unico blocco.

Nell'USR-Piemonte potrebbe essere usato per i contratti dei DS.

## Uso ##

Il programmetto vuole tre parametri:

`
dividiPDF.py ingresso.pdf tabella.csv ultima.pdf
`

Il file `ingresso.pdf` deve essere il file da suddividere.

La `tabella.csv` contiene le indicazioni, una per riga, relative alle
diverse sezioni da separare, in particolare due informazioni:
`Pagine`, il numero di pagine da inserire in questa sezione; `Nome` il
nome da dare al file, al nome viene aggiunta l'estensione '.pdf'.

Il file `ultima.pdf` contiene una sola pagina (comunque viene
considerata solamente la prima) da aggiungere in coda ad ognuna delle
sezioni.

## Attenzioni ##

Il programma *NON* effettua alcun controllo, sovrascrive i file che
vengono creati, anche se questi fossero in posizioni del disco diverse
rispetto a dove il programma viene lanciato.