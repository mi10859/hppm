#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##it
#    dividiPDF, suddivide un file PDF in diversi file, prendendo il
#    numero di pagine e il nome da dare alle diverse porzioni da un
#    CSV; ad ognuna delle porzioni viene anche aggiunta una singola
#    pagina da un secondo file.
#
#    Scritto nel 2017 da Marco BODRATO, che ne detiene i diritti d'autore
#
#    Questo programma è software libero: può essere ridistribuito o
#    modificato nei termini della licenza "GNU Affero General Public
#    License" come pubblicata dalla Free Software Foundation; o la
#    versione 3 della licenza, o (a vostra scelta) qualunque versione
#    successiva.
#
#    Questo programma è distribuito con l'auspicio che sia utile,
#    ma SENZA ALCUNA GARANZIA; senza neppure la garanzia implicita di
#    VENDIBILITÀ o ADEGUATEZZA AD UN PARTICOLARE SCOPO. Si veda la
#    "GNU Affero General Public License" per ulteriori dettagli.
#
#    Dovreste aver ricevuto una copia della "GNU Affero General Public
#    License" con a questo programma.
#    Vedere altrimenti <http://www.gnu.org/licenses/>.
#
##eo
#    dividiPDF, 
#    (C) 2017 Marco BODRATO
#
#    Ĉi tiu verko estas libera programo; vi povas ĝin pludistribui aŭ
#    modifi je la kondiĉoj de la GNUa Ĝenerala Publika Permesilo de
#    Affero, eldonita de «Free Software Foundation», laŭ la versio 3
#    de tiu Permesilo aŭ (se vi preferas) ajna posta versio.
# 
#    Ni distribuas ĉi tiun programon esperante ke ĝi estos utila,
#    tamen SEN IA AJN GARANTIO, i.a. sen la implica garantio pri
#    SURMERKATIGEBLO aŭ TAŬGECO POR IU KONKRETA CELO.  Pliajn detalojn
#    vidu en la GNUa Ĝenerala Publika Permesilo de Affero.
# 
#    Ekzemplero de la GNUa Ĝenerala Publika Permesilo de Affero devas
#    esti liverita al vi kun ĉi tiu programo; se vi ĝin ne ricevis,
#    rigardu <http://www.gnu.org/licenses/>.
#
##en
#    dividiPDF, 
#    Copyright (C) 2017 Marco BODRATO
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU Affero General Public
#    License as published by the Free Software Foundation, either
#    version 3 of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public
#    License along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#

from PyPDF2 import PdfFileReader, PdfFileWriter
import sys
import csv

if len(sys.argv) == 4:
  nomo2 = sys.argv[3]
  en2 = PdfFileReader (open (nomo2, 'rb'))
elif len(sys.argv) != 3:
  print ('Uso: dividiPDF.py <ingresso.pdf> <tabella.csv> [ultima.pdf]')
  sys.exit(-1)
else:
  en2 = False
nomeFileDati = sys.argv[2]
nomoDividendo = sys.argv[1]

separatoreDati = ';'
csvPorzioni = csv.DictReader(open (nomeFileDati), delimiter = separatoreDati)

en1 = PdfFileReader (open (nomoDividendo, 'rb'))
contapagine = 0

for riga in csvPorzioni:
  elDosiero = PdfFileWriter()
  elDosiernomo = riga['Nome'] + '.pdf'
  for i in range (int(riga['Pagine'])):
    elDosiero.addPage (en1.getPage (contapagine))
    contapagine += 1
  if en2 != False:
    elDosiero.addPage (en2.getPage (0))
  print ('(Sovra)scrivo ', elDosiernomo)
  el = open (elDosiernomo, 'wb')
  elDosiero.write (el)
  el.close ()
