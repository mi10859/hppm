# Trattamento dati per ore IRC #

File ausiliari per il trattamento dei dati relativi alle ore IRC.

Contiene piccoli, grezzi script in Python-3 per trasformare i file txt forniti da SIDI in file CSV gestibili con un foglio di calcolo.
