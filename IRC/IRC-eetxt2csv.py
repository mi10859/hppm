# IRC-eetxt2csv.py
#
# Grezzo programmino ausiliario per trasformare i file txt forniti da
# SIDI sulle ore di IRC per plesso di ogni provincia in file CSV
# lavorabili con un foglio elettronico.
#
# Presuppone che nel file IRC-ee.txt nella directory corrente sia
# presente il file scaricato da SIDI al quale siano state tolte le
# righe "spurie" iniziali e quelle finali. Ovverosia che il file sia
# una sequenza di blocchi di 6 righe nella forma ('#' iniziali
# eclusi):
#
# ! meccanogr.   XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX    XXXXXXXXXXXXXXXXXX              num__telefono !
# !           XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX   AGGREGANTE: mecc_aggr                         !
# !           ORE ESPLETATE DAI DOCENTI DI RELIGIONE CATTOLICA : ore,0                                    !
# !           XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX                !
# !           XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX                !
# !------------+------------------------------------------+-------------------------------+---------------!
#

import csv

nomeFileTxt = 'IRC-ee.txt'
nomeFileCsv = 'IRC-ee.csv'

fi = open (nomeFileTxt)
fo = open (nomeFileCsv, 'w')

risultato = csv.writer (fo, delimiter = '\t', quoting = csv.QUOTE_MINIMAL)
risultato.writerow(['Meccanografico','Telefono','Aggregatore','Ore'])

i = 0
mecc = ''
tel = ''
aggr = ''
ore = '0'
for line in iter(fi):
    if i == 0:
        mecc = line[3:13]
        tel = line[90:105]
    elif i == 1:
        aggr = line [71:81]
    elif i == 2:
        ore = line [64:71]
        risultato.writerow([mecc,tel,aggr,ore])
    i = i + 1;
    if i == 6:
        i = 0

fi.close()
fo.close()
