# Intenti #

Programmini ausiliari per... Me? MI1*? Mah...

La sigla scelta come nome (HPpM) viene dall'esperanto:
*HelpoProgrametoj pro M...*

Le consonanti in esperanto si chiamano postponendo la vocale "o",
quindi questa sigla che in italiano si leggerebbe AccaPiPiEmme,
in esperanto si legge Hopopomo, con l'acca aspirata.

Ma noi siam forse esperantisti? No! Non ci resta quindi che pronunciare
la sigla "Opopomoz", come omaggio al bel film di animazione.

## Programmetti ##

Si intende inserire in questo archivio una collezione di programmetti che
via via verranno utili... per uso comune e condivisione.

 * Un programmetto per suddividere diversi documenti digitalizzati in un unico blocco (contratti?) [dividiPDF](dividiPDF)
 * Quattro risibili abbozzi di supporto per rendere lavorabili i monitoraggi 'txt' sugli [IRC](IRC).